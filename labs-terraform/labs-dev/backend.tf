terraform {
  backend "s3" {
    bucket         = "iac-terraform-dev/"
    encrypt        = true
    key            = "backend/terraform"
    region         = "us-west-2"
  }
}