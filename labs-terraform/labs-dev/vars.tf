#############################BBCE###############################################
variable "LINK_Labs_1" {
  type    = list
  default = ["177.92.85.90/32"]
}

variable "LINK_Labs_2" {
  type    = list
  default = ["201.6.228.199/32"]
}

variable "WIFI_Labs" {
  type    = list
  default = ["177.103.187.75/32"]
}

variable "CIDRVPC_SG" {
  type = list
  default = ["10.27.0.0/16"]
}

#############################VPC###############################################
variable "AWS_REGION" {
  type    = string
  default = "us-west-2"
}

variable "CIDRVPC" {
  type    = string
  default = "10.27.0.0/16"
}

variable "requerente" {
  type    = string
  default = "Caio Monteiro"

}

variable "map_public_true" {
  type    = string
  default = "true"
}

variable "Labs_DMZ_A" {
  type    = string
  default = "us-west-2a"
}

variable "CIDR_Labs_DMZ_A" {
  type    = string
  default = "10.27.0.0/19"
}

variable "Labs_DMZ_B" {
  type    = string
  default = "us-west-2b"
}

variable "CIDR_Labs_DMZ_B" {
  type    = string
  default = "10.27.32.0/19"
}

variable "Labs_DMZ_C" {
  type    = string
  default = "us-west-2c"
}

variable "CIDR_Labs_DMZ_C" {
  type    = string
  default = "10.27.64.0/19"
}

variable "Labs_DMZ_D" {
  type    = string
  default = "us-west-2d"
}

variable "CIDR_Labs_DMZ_D" {
  type    = string
  default = "10.27.96.0/19"
}

variable "Labs_APP_A" {
  type    = string
  default = "us-west-2a"
}

variable "CIDR_Labs_APP_A" {
  type    = string
  default = "10.27.128.0/19"
}

variable "Labs_APP_B" {
  type    = string
  default = "us-west-2b"
}

variable "CIDR_Labs_APP_B" {
  type    = string
  default = "10.27.160.0/19"
}

variable "Labs_APP_C" {
  type    = string
  default = "us-west-2c"
}

variable "CIDR_Labs_APP_C" {
  type    = string
  default = "10.27.192.0/19"
}

variable "Labs_APP_D" {
  type    = string
  default = "us-west-2d"
}

variable "CIDR_Labs_APP_D" {
  type    = string
  default = "10.27.224.0/19"
}

variable "CIDR_Labs" {
  type    = string
  default = "10.27.0.0/16"
}

variable "instance_tenancy" {
  type    = string
  default = "default"
}

variable "enable_dns_support" {
  type    = string
  default = "true"
}

variable "enable_dns_hostnames" {
  type    = string
  default = "true"
}

variable "enable_classiclink" {
  type    = string
  default = "false"

}

variable "name_vpc" {
  type    = string
  default = "Labs"
}

variable "ENV" {
  type    = string
  default = "dev"
}

variable "CIDR_SHARED_SERVICE" {
  type    = string
  default = "10.57.0.0/16"
}

variable "AVAILABILITY_ZONES" {
  type = list
  default = ["us-west-2a", "us-west-2b", "us-west-2c", "us-west-2d"]
}

########################################VPN########################################
variable "CIDR_VPN_DESTINATION_office" {
  type    = list
  default = ["10.7.0.0/16"]
}

variable "COSTUMER_GTW_VPN" {
  type    = string
  default = "cgw-0f0fdec82aaaa0fc0"
}


#######################SQS#########################

variable "SQS_NAME" {
  type    = string
  default = "Labs"
}

variable "SQS_Labs" {
  type    = string
  default = "Negociacao"
}

variable "SQS_INTEGRACAO_API" {
  type    = string
  default = "NegociacaoApi"
}

variable "SQS_INTEGRACAO_API_MENSAGENS_COM_ERRO" {
  type    = string
  default = "NegociacaoApiMensagensComErro"
}

variable "SQS_NOTIFICACAO" {
  type    = string
  default = "Notificacao"
}


#################RDS#######################

variable "identifier" {
  type    = string
  default = "bbce-aurora-desenvolvimento-Labs"

}

variable "RANGE_IPS_SR" {
  type    = list
  default = ["11.17.0.0/19", "11.17.128.0/19", "11.17.160.0/19", "11.17.192.0/19", "11.17.224.0/19", "11.17.40.0/24", "11.17.60.0/24", "11.17.90.0/24", "11.17.30.0/24"]

}


variable "engine" {
  type    = string
  default = "aurora"

}

variable "engine_version" {
  type    = string
  default = "5.6.10a"
}


variable "instance_class" {
  type    = string
  default = "db.t3.medium"
}


variable "allocated_storage" {
  default = 100
}


variable "storage_encrypted" {
  type    = string
  default = "true"
}


variable "name_db" {
  type    = string
  default = "Labs"
}


variable "username" {
  type    = string
  default = "bbce"
}


variable "password" {
  type    = string
  default = "f00190a6e1"
}


variable "port" {
  default = 5432
}


variable "name-rds" {
  type    = string
  default = "Labs-subnet-group"

}

variable "username-rds" {
  type    = string
  default = "root"

}
variable "maintenance_window" {
  type    = string
  default = "Mon:00:00-Mon:03:00"
}


variable "backup_window" {
  type    = string
  default = "03:00-06:00"
}


variable "family" {
  type    = string
  default = "aurora5.6"
}


variable "final_snapshot_identifier" {
  type    = string
  default = "demodb"
}

#variable "storage_type" {
  #type    = string
  #default = "gp2"
#}

variable "delete_automated_backups" {
  type    = string
  default = "false"
}

variable "backup_retention_period_cluster" {
  default = 30

}

variable "multi_az" {
  type    = string
  default = "true"

}

variable "master_username_cluster" {
  type    = string
  default = "admin"

}

variable "master_password_cluster" {
  type    = string
  default = "5c87566ef7"

}

variable "COUNT" {
  type    = string
  default = "1"

}

variable "preferred_backup_window_cluster" {
  type    = string
  default = "01:00-02:00"
}

variable "name" {
  type    = string
  default = "Labs" 
}

variable "name-cluster" {
  type   = string
  default = "bbce-Labs"
}

variable "name_database" {
  type    = string
  default = "bbce_api"
}
###############################ALB###################


variable "ALB_NAME" {
  default = "Labs_nodes_"
}

variable "INTERNAL_ALB" {
  default = true
}

variable "EXTERNAL_ALB" {
  default = false
}

variable "IDLE_TIMEOUT" {
  default = "60"
}

variable "TYPEALB" {
  default = "application"
}

variable "PRIORITY" {
  default = "100"
}

variable "ALB_PATH_NEGOCIACAO" {
  type    = list
  default = ["/engine/negociacao/addAndMatch","/engine/negociacao/supender_todas","/engine/negociacao/supender_oferta","/engine/negociacao/syncEdit","/negociacao/*"]
}

variable "TARGET_GROUP_NAME_FULL" {
  type    = string
  default = "Labs-full"
}

variable "TARGET_GROUP_NAME_NEGOCIACAO" {
  type    = string
  default = "Labs-negociacao"
}

variable "TARGET_GROUP_NAME_ENGINE" {
  type    = string
  default = "Labs-engine"
}

variable "TARGET_GROUP_PATH" {
  default = "/"
}

variable "TARGET_GROUP_PATH_ENGINE" {
  default = "/matchengine"
}

variable "ENV-ALB" {
  type    = string
  default = "Labs-API"
}

variable "ENV-ALB_ENGINE" {
  type    = string
  default = "Engine"
}

variable "ENVIO" {
  type    = string
  default = "dev"

}

variable "INSTANCE_CLASS_RDS" {
  type    = string
  default = "db.t3.medium"
}

######################schedule################

variable "scheduled_action_name_start" {
  type    = string
  default = "start"

}

variable "scheduled_action_name_stop" {
  type    = string
  default = "stop"

}

variable recurrence_start {
  type        = string
  default     = "0 11 * * MON-FRI"
  description = "Hora para ligar maquinas"
}


variable recurrence_stop {
  type        = string
  default     = "0 22 * * MON-FRI"
  description = "Hora para deligar maquinas"
}



######################CloudFront################
variable BUCKET_S3_CLOUDFRONT_NAME {
  type = string
  default = "Labs-dev.bbce.tech"
  description = "Nome do bucket S3 onde fica o código do front"
}

variable BUCKET_S3_CLOUDFRONT_ORIGIN_ID {
  type = string
  default = "LabsdevS3Id"
  description = "Id do bucket S3 onde fica o código do front"
}

variable CLOUDFRONT_COMMENT {
  type = string
  default = "Front do balcão ambiente de dev"
  description = "Comentário sobre o CloudFront"
}

variable CLOUDFRONT_DEFAULT_ROOT_OBJECT {
  type = string
  default = "index.html"
}

variable CLOUDFRONT_LOGGING_BUCKET {
  type = string
  default = ""
}

variable CLOUDFRONT_LOGGING_PREFIX {
  type = string
  default = ""
}

variable CLOUDFRONT_CNAMES {
  type = list
  default = ["Labs-dev.bbce.tech"]
}

variable CLOUDFRONT_NAMES {
  type = string
  default = "Labs-dev.bbce.tech"
}

variable FORWARD_COOKIES {
  type = string
  default = "whitelist"
}

variable FORWARD_COOKIES_WHITELISTED_NAMES {
  type = list
  default = ["Origin"]
}

variable BBCE_CERTIFICATE_ARN {
  type = string
  default = "arn:aws:acm:us-east-1:822956234201:certificate/f0ac4661-3e25-4805-82bd-7f124b27a32f"
}

variable BBCE_CERTIFICATE {
  type = string
  default = "arn:aws:acm:us-west-2:822956234201:certificate/142d3b30-1520-4f30-86e9-e3b2673f40d4"
}

variable CLOUDFRONT_CERTIFICATE_MIN_PROTOCOL_VERSION {
  type = string
  default = "TLSv1.2_2018"
}

variable LOCATIONS_CLOUDFRONT {
  type = string
  default = " "
}

variable RESTRICTION_CLOUDFRONT {
  type = string
  default = "none"
}

variable SSL_METHOD {
  type = string
  default = "sni-only"
}

################################s3#############################

variable BUCKET_S3_DOCS_Labs  {
  type  = string
  default = "negociacao-dev"
}

######################REDIS################

variable REDIS_NODE_TYPE {
  type = string
  default = "cache.t2.medium"
}

variable REDIS_NUM_CACHE_NODES {
  type = number
  default = 1
}

variable REDIS_PARAMETER_GROUP_NAME {
  type = string
  default = "default.redis5.0"
}

variable REDIS_ENGINE_VERSION {
  type = string
  default = "5.0.6"
}

variable REDIS_PORT {
  type = number
  default = 6379
}

variable REDIS_APPLY_IMMEDIATELY {
  type = bool
  default = true
}

variable REDIS_CLUSTER_ID {
  type = string
  default = "redis-signal-dev"
}

###########LAUNCH TEMPLATE############

variable LT_NAME_FUL {
  type = string
  default = "LT_Labs-FULL"
}

variable LT_NAME_NEGOCIACAO {
  type = string
  default = "LT_Labs-NEGOCIACAO"
}

variable LT_NAME_ENGINE {
  type = string
  default = "LT_Labs-ENGINE"
}

variable LT_DEVICE_NAME {
  type = string
  default = "/dev/sda1"
}

variable LT_VOLUME_TYPE {
  type = string
  default = "gp2"
}

variable  LT_INSTANCE_PROFILE_NAME {
  type = string
  default = "CodeDeploy-InstanceProfile"
}

variable  LT_AMI_ID_Labs {
  type = string
  default = "ami-06e19a29f91b2bca2"
}

variable  LT_AMI_ID_NEGOCIACAO {
  type = string
  default = "ami-06e19a29f91b2bca2"
}

variable  LT_AMI_ID_ENGINE {
  type = string
  default = "ami-0fc6269234e1ae1e4"
}

variable  LT_INSTANCE_TYPE {
  type = string
  default = "c5.large"
}

variable LT_KEY_NAME {
  type = string
  default = "bbce-dev"
}

variable LT_MONITORING_ENABLED {
  type = bool
  default = "true"
}

variable LT_SECURITY_GROUP {
  type = list
  default = ["aws_security_group.appSG.id"]
}

#######################Auto Scalling################

variable ASG_NAME_NEGOCIACAO {
  type = string
  default = "Labs-negociacao"
}

variable ASG_NAME_FULL {
  type = string
  default = "Labs-full"
}

variable ASG_NAME_ENGINE {
  type = string
  default = "Labs-engine-dev"
}

variable ASG_DESIRED_CAPACITY_FULL {
  type = number
  default = "1"
}

variable ASG_MAX_SIZE_FULL {
  type = number
  default = "1"
}

variable  ASG_MIN_SIZE_FULL {
  type = number
  default = "1"
}

variable ASG_DESIRED_CAPACITY_NEGOCIACAO {
  type = number
  default = "1"
}

variable ASG_MAX_SIZE_NEGOCIACAO {
  type = number
  default = "1"
}

variable  ASG_MIN_SIZE_NEGOCIACAO {
  type = number
  default = "1"
}

variable ASG_DESIRED_CAPACITY_ENGINE {
  type = number
  default = "1"
}

variable ASG_MAX_SIZE_ENGINE {
  type = number
  default = "1"
}

variable  ASG_MIN_SIZE_ENGINE {
  type = number
  default = "1"
}





