resource "aws_security_group" "lbSG-engine" {
  name   = "BBCE-ENGINE-LB-${var.ENV}"
  vpc_id = "${var.vpc_id}"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_security_group_rule" "web-engine" {
  security_group_id = "${aws_security_group.lbSG-engine.id}"
  type              = "ingress"
  from_port         = 10000
  to_port           = 10000
  protocol          = "tcp"
  cidr_blocks       = var.CIDRVPC_SG
}

