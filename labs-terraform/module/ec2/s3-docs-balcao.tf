resource "aws_s3_bucket" "s3-Labs" {
  bucket = var.BUCKET_S3_DOCS_Labs 
  acl    = "private"
  policy = "${file("${path.module}/policy.json")}"



  tags = {
    Name                             = var.BUCKET_S3_DOCS_Labs 
    Terraform                        = "true"
    Environment                      = var.ENV
    APP                              = "Labs"
    Projeto                          = "Labs"
    Requerente                       = var.requerente
  }
}

resource "aws_s3_bucket_public_access_block" "s3-acessblock" {
  bucket = "${aws_s3_bucket.s3-Labs.id}"

  block_public_acls   = false
  block_public_policy = false
}