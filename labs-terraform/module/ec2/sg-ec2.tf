resource "aws_security_group" "appSG" {
  name   = "BBCE-Labs-${var.ENV}"
  vpc_id = "${var.vpc_id}"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_security_group_rule" "vpc" {
  security_group_id = "${aws_security_group.appSG.id}"
  type              = "ingress"
  from_port         = "0"
  to_port           = "0"
  protocol          = "-1"
  cidr_blocks       = var.CIDRVPC_SG
  description       = "VPC"
}

resource "aws_security_group_rule" "vpn" {
  security_group_id = "${aws_security_group.appSG.id}"
  type              = "ingress"
  from_port         = "0"
  to_port           = "0"
  protocol          = "-1"
  cidr_blocks       = "${var.CIDR_VPN_DESTINATION_office}"
  description       = "VPN"
}

resource "aws_security_group_rule" "wifi_bbce" {
  security_group_id = "${aws_security_group.appSG.id}"
  type              = "ingress"
  from_port         = "0"
  to_port           = "0"
  protocol          = "-1"
  cidr_blocks       = "${var.WIFI_BBCE}"
  description       = "WIFI BBCE"
}

resource "aws_security_group_rule" "link1_bbce" {
  security_group_id = "${aws_security_group.appSG.id}"
  type              = "ingress"
  from_port         = "3389"
  to_port           = "3389"
  protocol          = "tcp"
  cidr_blocks       = "${var.LINK_BBCE_1}"
  description       = "LINK BBCE 1"
}

resource "aws_security_group_rule" "link2_bbce" {
  security_group_id = "${aws_security_group.appSG.id}"
  type              = "ingress"
  from_port         = "3389"
  to_port           = "3389"
  protocol          = "tcp"
  cidr_blocks       = "${var.LINK_BBCE_2}"
  description       = "LINK BBCE 2"
}
