resource "aws_alb" "alb" {
  name            = "${var.ENV-ALB}-${var.ENVIO}"
  subnets         = var.LISTASBPUBLIC
  security_groups = ["${aws_security_group.lbSG.id}"]
  internal        = var.EXTERNAL_ALB
  idle_timeout    = var.IDLE_TIMEOUT
  load_balancer_type = var.TYPEALB

  tags = {
    Name    =  "${var.ENV-ALB}-${var.ENVIO}"
  }
}

resource "aws_alb_target_group" "alb_target_group_negociacao" {
  name     = "${var.TARGET_GROUP_NAME_NEGOCIACAO}-${var.ENVIO}"
  port     = 80
  protocol = "HTTP"
  vpc_id   = var.vpc_id

  health_check {
    healthy_threshold   = 5
    unhealthy_threshold = 2
    timeout             = 5
    interval            = 30
    matcher             = 200
    path                = var.TARGET_GROUP_PATH_ENGINE
    protocol            = "HTTP" 
  }
}
  resource "aws_alb_target_group" "alb_target_group_full" {
  name     = "${var.TARGET_GROUP_NAME_FULL}-${var.ENVIO}"
  port     = 80
  protocol = "HTTP"
  vpc_id   = var.vpc_id
  
 
  health_check {
    healthy_threshold   = 5
    unhealthy_threshold = 2
    timeout             = 5
    interval            = 30
    matcher             = 200
    path                = var.TARGET_GROUP_PATH
    protocol            = "HTTP"
  }
}


resource "aws_alb_listener" "alb_listener" {
  load_balancer_arn = aws_alb.alb.arn
  port              =  443
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = var.BBCE_CERTIFICATE

  default_action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.alb_target_group_full.arn
  }
}

resource "aws_alb_listener_rule" "listener_rule" {
  listener_arn = "${aws_alb_listener.alb_listener.arn}"
  priority     = var.PRIORITY
  action {
    type             = "forward"
    target_group_arn = "${aws_alb_target_group.alb_target_group_negociacao.id}"
  }
  condition {
    path_pattern {
      values = var.ALB_PATH_NEGOCIACAO

  }
 }
}
resource "aws_alb_listener_certificate" "certificate_bbce" {
  listener_arn = aws_alb_listener.alb_listener.arn
  certificate_arn = var.BBCE_CERTIFICATE
}
