resource "aws_autoscaling_group" "asgNegociacao" {
  name               = var.ASG_NAME_NEGOCIACAO
  vpc_zone_identifier = var.ASG_VPC_ZONE_PR
  desired_capacity   = var.ASG_DESIRED_CAPACITY_NEGOCIACAO
  max_size           = var.ASG_MAX_SIZE_NEGOCIACAO
  min_size           = var.ASG_MIN_SIZE_NEGOCIACAO

  tag  {
    
    key                              = "Name"
    value                            = "Labs-Negociacao"
    propagate_at_launch              = true
  }

  launch_template {
    id      = "${aws_launch_template.launch_negociacao.id}"
    version = "$Latest"
  }
}

resource "aws_autoscaling_schedule" "start_NEGOCIACAO_scheduled_action_name" {
  scheduled_action_name  = var.scheduled_action_name_start
  min_size               = 1
  max_size               = 1
  desired_capacity       = 1
  recurrence             = var.recurrence_start
  autoscaling_group_name = "${aws_autoscaling_group.asgNegociacao.name}"
}

resource "aws_autoscaling_schedule" "stop_NEGOCIACAO_scheduled_action_name" {
  scheduled_action_name  = var.scheduled_action_name_stop
  min_size               = 0
  max_size               = 0
  desired_capacity       = 0 
  recurrence             = var.recurrence_stop
  autoscaling_group_name = "${aws_autoscaling_group.asgNegociacao.name}"

}


resource "aws_autoscaling_attachment" "asg_attachment_negociacao" {
  autoscaling_group_name = "${aws_autoscaling_group.asgNegociacao.id}"
  alb_target_group_arn   = "${aws_alb_target_group.alb_target_group_negociacao.arn}"

}

resource "aws_autoscaling_group" "asgFull" {
  name               = var.ASG_NAME_FULL
  vpc_zone_identifier = var.ASG_VPC_ZONE_PR
  desired_capacity   = var.ASG_DESIRED_CAPACITY_FULL
  max_size           = var.ASG_MAX_SIZE_FULL
  min_size           = var.ASG_MIN_SIZE_FULL

  tag  {

    key                              = "Name"
    value                            = "Labs-Full"
    propagate_at_launch              = true
  }

  launch_template {
    id      = "${aws_launch_template.launch_full.id}"
    version = "$Latest"
  }
}

resource "aws_autoscaling_schedule" "start_Labs_scheduled_action_name" {
  scheduled_action_name  = var.scheduled_action_name_start
  min_size               = 1
  max_size               = 1
  desired_capacity       = 1
  recurrence             = var.recurrence_start
  autoscaling_group_name = "${aws_autoscaling_group.asgFull.name}"
}

resource "aws_autoscaling_schedule" "stop_Labs_scheduled_action_name" {
  scheduled_action_name  = var.scheduled_action_name_stop
  min_size               = 0
  max_size               = 0
  desired_capacity       = 0 
  recurrence             = var.recurrence_stop
  autoscaling_group_name = "${aws_autoscaling_group.asgFull.name}"

}

resource "aws_autoscaling_attachment" "asg_attachment_full" {
  autoscaling_group_name = "${aws_autoscaling_group.asgFull.id}"
  alb_target_group_arn   = "${aws_alb_target_group.alb_target_group_full.arn}"

}


resource "aws_autoscaling_group" "asgENGINE" {
  name               = var.ASG_NAME_ENGINE
  vpc_zone_identifier = var.ASG_VPC_ZONE_PR
  desired_capacity   = var.ASG_DESIRED_CAPACITY_ENGINE
  max_size           = var.ASG_MAX_SIZE_ENGINE
  min_size           = var.ASG_MIN_SIZE_ENGINE

  tag  {
    
    key                              = "Name"
    value                            = "Labs-Engine"
    propagate_at_launch              = true
  }
  
  launch_template {
    id      = "${aws_launch_template.launch_engine.id}"
    version = "$Latest"
  }
}

resource "aws_autoscaling_schedule" "start_ENGINE_scheduled_action_name" {
  scheduled_action_name  = var.scheduled_action_name_start
  min_size               = 1
  max_size               = 1
  desired_capacity       = 1
  recurrence             = var.recurrence_start
  autoscaling_group_name = "${aws_autoscaling_group.asgENGINE.name}"
}

resource "aws_autoscaling_schedule" "stop_ENGINE_scheduled_action_name" {
  scheduled_action_name  = var.scheduled_action_name_stop
  min_size               = 0
  max_size               = 0
  desired_capacity       = 0 
  recurrence             = var.recurrence_stop
  autoscaling_group_name = "${aws_autoscaling_group.asgENGINE.name}"

}

resource "aws_autoscaling_attachment" "asg_attachment_engine" {
  autoscaling_group_name = "${aws_autoscaling_group.asgENGINE.id}"
  alb_target_group_arn   = "${aws_alb_target_group.alb_target_group_engine.arn}"

}
