resource "aws_iam_user" "appLabs" {
  name = "appLabs"
  path = "/"

  tags = {
    Name                             = "appLabs-${var.ENV}"
    Terraform                        = "true"
    Environment                      = "${var.ENV}"
    APP                              = "Labs"
    Projeto                          = "Labs"
    Requerente                       = "${var.requerente}"
    Role                             = "ACESSO"
  }
}

resource "aws_iam_access_key" "appLabs" {
  user = "${aws_iam_user.appLabs.name}"
}

//TODO rever policy
resource "aws_iam_user_policy" "appLabs" {
  user = "${aws_iam_user.appLabs.name}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:Get*",
        "s3:List*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}