resource "aws_launch_template" "launch_full" {
  name = var.LT_NAME_FUL

  block_device_mappings {
    device_name       = var.LT_DEVICE_NAME
    

  ebs {
      volume_size = 30
      delete_on_termination = true
      volume_type = var.LT_VOLUME_TYPE
      encrypted = false
    }
  }
  capacity_reservation_specification {
    capacity_reservation_preference = "none"
  }


  iam_instance_profile {
    name = var.LT_INSTANCE_PROFILE_NAME
  }

  image_id = var.LT_AMI_ID_Labs

  instance_type = var.LT_INSTANCE_TYPE


  key_name = var.LT_KEY_NAME


  monitoring {
    enabled = var.LT_MONITORING_ENABLED
  }

  vpc_security_group_ids = [aws_security_group.appSG.id]

  tag_specifications {
    resource_type = "instance"

    tags = {
      Sistema = "Labs"
      Ambiente = "${var.ENV}"
      APP = "Labs"
    }
  }

  user_data = "${base64encode(data.template_file.myuserdata.rendered)}"
}