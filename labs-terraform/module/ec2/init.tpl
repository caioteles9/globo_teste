<powershell>
$bucket = "bbce-ec2-scripts-dev"
$region = "us-west-2"
$localPath = "C:\temp\scriptsec2\"
New-Item -ItemType Directory -Force -Path $localPath
$localFileName = Join-Path $localPath "newInstancedevLabs.ps1"
Read-S3Object -BucketName $bucket -Key newInstancedevLabs.ps1 -Region $region -File $localFileName
& $localFileName
</powershell>