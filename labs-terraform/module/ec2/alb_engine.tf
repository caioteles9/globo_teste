resource "aws_alb" "alb_engine" {
  name            = "${var.ENV-ALB_ENGINE}-${var.ENVIO}"
  subnets         = var.LISTASBPRIVATE
  security_groups = ["${aws_security_group.lbSG-engine.id}"]
  internal        = var.INTERNAL_ALB
  idle_timeout    = var.IDLE_TIMEOUT
  load_balancer_type = var.TYPEALB

}

resource "aws_alb_listener" "alb_listener_engine" {
  load_balancer_arn = aws_alb.alb_engine.arn
  port              =  10000
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_alb_target_group.alb_target_group_engine.arn
    type             = "forward"
  }
}

resource "aws_alb_target_group" "alb_target_group_engine" {
  name     = "${var.TARGET_GROUP_NAME_ENGINE}-${var.ENVIO}"
  port     = 10000
  protocol = "HTTP"
  vpc_id   = var.vpc_id
  
  health_check {
    healthy_threshold   = 5
    unhealthy_threshold = 2
    timeout             = 5
    interval            = 30
    path                = var.TARGET_GROUP_PATH_ENGINE
    port                = 80
  }
}


