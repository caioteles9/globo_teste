variable "ENV" {}
variable "CIDRVPC" {}
variable "CIDRVPC_SG" {}
variable "AVAILABILITY_ZONES" {}
variable "CIDR_VPN_DESTINATION_office" {}
variable "LINK_BBCE_1" {}
variable "LINK_BBCE_2" {}
variable "WIFI_BBCE" {}
variable "CIDR_Labs" {}
variable "CIDR_SHARED_SERVICE" {}
variable "LT_NAME_FUL" {}
variable "LT_NAME_NEGOCIACAO" {}
variable "LT_NAME_ENGINE" {}
variable "LT_AMI_ID_Labs" {}
variable "LT_AMI_ID_ENGINE" {}
variable "LT_DEVICE_NAME" {}
variable "LT_VOLUME_TYPE" {}
variable "LT_INSTANCE_PROFILE_NAME" {}
variable "LT_INSTANCE_TYPE" {}
variable "LT_KEY_NAME" {}
variable "LT_MONITORING_ENABLED" {}
variable "LT_SECURITY_GROUP" {}
variable "LT_AMI_ID_NEGOCIACAO"{}
variable "ASG_NAME_NEGOCIACAO" {}
variable "ASG_NAME_FULL" {}
variable "ASG_NAME_ENGINE" {}
variable "ASG_DESIRED_CAPACITY_FULL" {}
variable "ASG_MAX_SIZE_FULL" {}
variable "ASG_MIN_SIZE_FULL" {}
variable "ASG_DESIRED_CAPACITY_NEGOCIACAO" {}
variable "ASG_MAX_SIZE_NEGOCIACAO" {}
variable "ASG_MIN_SIZE_NEGOCIACAO" {}
variable "ASG_MIN_SIZE_ENGINE" {}
variable "ASG_MAX_SIZE_ENGINE" {}
variable "ASG_DESIRED_CAPACITY_ENGINE" {}
variable "ASG_VPC_ZONE_PR" {}

variable "requerente"     {}
variable "vpc_id"         {}
variable "RANGE_IPS_SR"{}
variable "ALB_NAME"               {}
variable "INTERNAL_ALB"           {}
variable "EXTERNAL_ALB"           {}
variable "IDLE_TIMEOUT"           {}
variable "TYPEALB"                {}
variable "PRIORITY"               {}
variable "ALB_PATH_NEGOCIACAO"    {}
variable "TARGET_GROUP_NAME_ENGINE" {}
variable "TARGET_GROUP_PATH"      {}
variable "TARGET_GROUP_PATH_ENGINE" {}
variable "LISTASBPRIVATE"          {}
variable "LISTASBPUBLIC"           {}
variable "BUCKET_S3_DOCS_Labs"   {}
variable "ENV-ALB" {}
variable "ENV-ALB_ENGINE" {}
variable "TARGET_GROUP_NAME_NEGOCIACAO" {}
variable "ENVIO" {}
variable "scheduled_action_name_start" {}
variable "scheduled_action_name_stop" {}
variable "recurrence_stop" {}
variable "recurrence_start" {}
variable "BBCE_CERTIFICATE" {}
variable "TARGET_GROUP_NAME_FULL" {}


variable "AWS_TYPE_INSTANCE"{
    type = string
    default = "m5a.large"
    description = "tipo da instancia"
}

variable "DEKS" {
    type = string
    default = "4"

}

variable "DMAXEKS" {
    type = string
    default = "8"

}

variable "DMIN" {
     type = string
    default = "4"
}