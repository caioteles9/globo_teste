resource "aws_s3_bucket" "s3-front" {
  bucket = var.BUCKET_S3_CLOUDFRONT_NAME
  acl    = "public-read"

 website {
    index_document = "index.html"
    error_document = "error.html"
  
  routing_rules = <<EOF
[{
    "Condition": {
        "HttpErrorCodeReturnedEquals": "404"
    },
    "Redirect": {
        "Protocol": "https",
        "HostName": "Labs.bbce.tech",
        "ReplaceKeyPrefixWith": "#!/"
    }
}]
EOF
  }

  tags = {
    Name                             = var.BUCKET_S3_CLOUDFRONT_NAME
    Terraform                        = "true"
    Environment                      = var.ENV
    APP                              = "Labs"
    Projeto                          = "Labs"
    Requerente                       = var.requerente
  }
}

locals {
  s3_origin_id = var.BUCKET_S3_CLOUDFRONT_ORIGIN_ID
}

resource "aws_cloudfront_distribution" "Cloudfront_Labs" {
  origin {
    domain_name = "${aws_s3_bucket.s3-front.website_endpoint}"
    origin_id   = var.BUCKET_S3_CLOUDFRONT_ORIGIN_ID

    custom_origin_config {
      http_port                = "80"
      https_port               = "443"
      origin_ssl_protocols     = ["TLSv1"]
      origin_protocol_policy   = "match-viewer"
      origin_read_timeout      = "30"
      origin_keepalive_timeout = "5"
    }
  }

  enabled             = true
  is_ipv6_enabled     = true
  comment             = var.CLOUDFRONT_COMMENT
  default_root_object = var.CLOUDFRONT_DEFAULT_ROOT_OBJECT

  aliases = var.CLOUDFRONT_CNAMES
  #aliases = ["mysite.example.com", "yoursite.example.com"]

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD", "OPTIONS"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = local.s3_origin_id

    forwarded_values {
      query_string = false

      cookies {
        forward = "${var.FORWARD_COOKIES}"
        whitelisted_names = var.FORWARD_COOKIES_WHITELISTED_NAMES
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400

  }

  price_class = "PriceClass_All"

  restrictions {
    geo_restriction {
      restriction_type = var.RESTRICTION_CLOUDFRONT
    }
  }

  tags = {
    Name                             = var.CLOUDFRONT_NAMES
    Terraform                        = "true"
    Environment                      = var.ENV
    APP                              = "Labs"
    Projeto                          = "Labs"
    Requerente                       = var.requerente
  }

#####Configurar Certificado em Virginia por Default ele busca dessa Região###

  viewer_certificate {
    acm_certificate_arn       =  var.BBCE_CERTIFICATE_ARN
    minimum_protocol_version  =  var.CLOUDFRONT_CERTIFICATE_MIN_PROTOCOL_VERSION
    ssl_support_method        =  var.SSL_METHOD
  }
}