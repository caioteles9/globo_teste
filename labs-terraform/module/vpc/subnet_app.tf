resource "aws_subnet" "vpc-private-0" {
  vpc_id                  = "${aws_vpc.vpc.id}"
  cidr_block              = "${var.CIDR_Labs_APP_A}"
  map_public_ip_on_launch = "false"
  availability_zone       = "${var.Labs_APP_A}"

  tags = {
    Name                             = "Labs_APP_A_${var.ENV}"
    Terraform                        = "true"
    Ambiente                         = "${var.ENV}"
    APP                              = "Labs"
    Projeto                          = "Labs"
    Requerente                       = "${var.requerente}"
    "kubernetes.io/cluster/eks-bbce" = "shared"
  }
}

resource "aws_subnet" "vpc-private-1" {
  vpc_id                  = "${aws_vpc.vpc.id}"
  cidr_block              = "${var.CIDR_Labs_APP_B}"
  map_public_ip_on_launch = "false"
  availability_zone       = "${var.Labs_APP_B}"

  tags = {
    Name                             = "Labs_APP_B_${var.ENV}"
    Terraform                        = "true"
    Ambiente                         = "${var.ENV}"
    APP                              = "Labs"
    Projeto                          = "Labs"
    Requerente                       = "${var.requerente}"
    "kubernetes.io/cluster/eks-bbce" = "shared"
  }
}


resource "aws_subnet" "vpc-private-2" {
  vpc_id                  = "${aws_vpc.vpc.id}"
  cidr_block              = "${var.CIDR_Labs_APP_C}"
  map_public_ip_on_launch = "false"
  availability_zone       = "${var.Labs_APP_C}"

  tags = {
    Name                             = "Labs_APP_C_${var.ENV}"
    Terraform                        = "true"
    Ambiente                         = "${var.ENV}"
    APP                              = "Labs"
    Projeto                          = "Labs"
    Requerente                       = "${var.requerente}"
    "kubernetes.io/cluster/eks-bbce" = "shared"
  }
}


resource "aws_subnet" "vpc-private-3" {
  vpc_id                  = "${aws_vpc.vpc.id}"
  cidr_block              = "${var.CIDR_Labs_APP_D}"
  map_public_ip_on_launch = "false"
  availability_zone       = "${var.Labs_APP_D}"

  tags = {
    Name                             = "Labs_APP_D_${var.ENV}"
    Terraform                        = "true"
    Ambiente                         = "${var.ENV}"
    APP                              = "Labs"
    Projeto                          = "Labs"
    Requerente                       = "${var.requerente}"
    "kubernetes.io/cluster/eks-bbce" = "shared"
  }
}