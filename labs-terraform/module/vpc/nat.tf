# nat gw
resource "aws_eip" "Labs-nat" {
  vpc = true
  tags = {
    Name                             = "eip_nat_Labs_${var.ENV}"
    Terraform                        = "true"
    Ambiente                         = "${var.ENV}"
    APP                              = "Labs"
    Projeto                          = "Labs"
    Requerente                       = "${var.requerente}"
    "kubernetes.io/cluster/eks-bbce" = "shared"
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_nat_gateway" "nat-gw" {
  allocation_id = "${aws_eip.Labs-nat.id}"
  subnet_id     = "${aws_subnet.vpc-public-0.id}"
  depends_on    = ["aws_internet_gateway.vpc-gw"]
  tags = {
    Name                             = "nat_gtw_Labs_${var.ENV}"
    Terraform                        = "true"
    Ambiente                         = "${var.ENV}"
    APP                              = "Labs"
    Projeto                          = "Labs"
    Requerente                       = "${var.requerente}"
    "kubernetes.io/cluster/eks-bbce" = "shared"
  }

  lifecycle {
    create_before_destroy = true
  }
}
