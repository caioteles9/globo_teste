resource "aws_subnet" "vpc-public-0" {
  vpc_id                  = "${aws_vpc.vpc.id}"
  cidr_block              = "${var.CIDR_Labs_DMZ_A}"
  map_public_ip_on_launch = "true"
  availability_zone       = "${var.Labs_DMZ_A}"

  tags = {
    Name                             = "Labs_DMZ_A_${var.ENV}"
    Terraform                        = "true"
    Ambiente                         = "${var.ENV}"
    APP                              = "Labs"
    Projeto                          = "Labs"
    Requerente                       = "${var.requerente}"
    "kubernetes.io/cluster/eks-bbce" = "shared"
  }
}

resource "aws_subnet" "vpc-public-1" {
  vpc_id                  = "${aws_vpc.vpc.id}"
  cidr_block              = "${var.CIDR_Labs_DMZ_B}"
  map_public_ip_on_launch = "true"
  availability_zone       = "${var.Labs_DMZ_B}"

  tags = {
    Name                             = "Labs_DMZ_B_${var.ENV}"
    Terraform                        = "true"
    Ambiente                         = "${var.ENV}"
    APP                              = "Labs"
    Projeto                          = "Labs"
    Requerente                       = "${var.requerente}"
    "kubernetes.io/cluster/eks-bbce" = "shared"
  }
}

resource "aws_subnet" "vpc-public-2" {
  vpc_id                  = "${aws_vpc.vpc.id}"
  cidr_block              = "${var.CIDR_Labs_DMZ_C}"
  map_public_ip_on_launch = "true"
  availability_zone       = "${var.Labs_DMZ_C}"

  tags = {
    Name                             = "Labs_DMZ_C_${var.ENV}"
    Terraform                        = "true"
    Ambiente                         = "${var.ENV}"
    APP                              = "Labs"
    Projeto                          = "Labs"
    Requerente                       = "${var.requerente}"
    "kubernetes.io/cluster/eks-bbce" = "shared"
  }
}

resource "aws_subnet" "vpc-public-3" {
  vpc_id                  = "${aws_vpc.vpc.id}"
  cidr_block              = "${var.CIDR_Labs_DMZ_D}"
  map_public_ip_on_launch = "true"
  availability_zone       = "${var.Labs_DMZ_D}"

  tags = {
    Name                             = "Labs_DMZ_D_${var.ENV}"
    Terraform                        = "true"
    Ambiente                         = "${var.ENV}"
    APP                              = "Labs"
    Projeto                          = "Labs"
    Requerente                       = "${var.requerente}"
    "kubernetes.io/cluster/eks-bbce" = "shared"
  }
}
