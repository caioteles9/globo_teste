resource "aws_db_subnet_group" "Labs_subnet" {
  name        = var.name-rds
  subnet_ids  = var.subnet_ids
}

resource "aws_rds_cluster" "clusterLabs" {
  db_subnet_group_name    = aws_db_subnet_group.Labs_subnet.name
  cluster_identifier      = "${var.name-cluster}-${var.ENV}"
  database_name           = "${var.name_database}"
  master_username         = var.master_username_cluster
  master_password         = var.master_password_cluster
  backup_retention_period = var.backup_retention_period_cluster
  preferred_backup_window = var.preferred_backup_window_cluster
  skip_final_snapshot     = true                                        
  
  # skip final snapshot when doing terraform destroy

  tags = {
    Name                             = "${var.name}-cluster${var.ENV}"
    Terraform                        = "true"
    Ambiente                         = "${var.ENV}"
    APP                              = "Labs"
    Projeto                          = "Labs"
    Requerente                       = "${var.requerente}"
  }
}

resource "aws_rds_cluster_instance" "Labs1" {
  apply_immediately  = true
  cluster_identifier = aws_rds_cluster.clusterLabs.id
  identifier         = "${var.name-cluster}-${var.ENV}-1"
  instance_class     = var.INSTANCE_CLASS_RDS

  tags = {
    Name                             = "${var.name}-cluster${var.ENV}"
    Terraform                        = "true"
    Ambiente                         = var.ENV
    APP                              = "Labs"
    Projeto                          = "Labs"
    Requerente                       = var.requerente
  }
}

resource "aws_rds_cluster_endpoint" "eligible" {
  cluster_identifier          = aws_rds_cluster.clusterLabs.id
  cluster_endpoint_identifier = "reader"
  custom_endpoint_type        = "READER"

  excluded_members = [
    aws_rds_cluster_instance.Labs1.id
    
  ]
  tags = {
    Name                             = "${var.name}-cluster${var.ENV}"
    Terraform                        = "true"
    Ambiente                         = var.ENV
    APP                              = "Labs"
    Projeto                          = "Labs"
    Requerente                       = var.requerente
  }
}

resource "aws_rds_cluster_endpoint" "static" {
  cluster_identifier          = aws_rds_cluster.clusterLabs.id
  cluster_endpoint_identifier = "static"
  custom_endpoint_type        = "READER"

  static_members = [
    aws_rds_cluster_instance.Labs1.id
    
  ]
  tags = {
    Name                             = "${var.name}-cluster${var.ENV}"
    Terraform                        = "true"
    Ambiente                         = "${var.ENV}"
    APP                              = "Labs"
    Projeto                          = "Labs"
    Requerente                       = var.requerente
  }
}