resource "aws_elasticache_subnet_group" "subnet-redis" {
    
  name       = "Redis-Labs-subnets"
  subnet_ids = var.SUBNET_GROUP_NAME

}


resource "aws_elasticache_cluster" "REDIS" {
  cluster_id           = var.REDIS_CLUSTER_ID
  engine               = "redis"
  node_type            = var.REDIS_NODE_TYPE
  num_cache_nodes      = var.REDIS_NUM_CACHE_NODES
  parameter_group_name = var.REDIS_PARAMETER_GROUP_NAME
  engine_version       = var.REDIS_ENGINE_VERSION
  port                 = var.REDIS_PORT
  subnet_group_name    = aws_elasticache_subnet_group.subnet-redis.name
  security_group_ids   = var.REDIS_SECURITY_GROUP_NAMES
  apply_immediately    = var.REDIS_APPLY_IMMEDIATELY
}